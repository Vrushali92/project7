//
//  DetailViewController.swift
//  Project7
//
//  Created by Vrushali Kulkarni on 08/06/20.
//  Copyright © 2020 Vrushali Kulkarni. All rights reserved.
//

import UIKit
import WebKit

final class DetailViewController: UIViewController {

    private var webView: WKWebView!
    private var detailItem: Petition?

    init(detailItem: Petition?) {
        self.detailItem = detailItem
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        guard let detailItem = detailItem else { return }
        loadHTMLString(detailItem: detailItem)
    }

    override func loadView() {
        webView = WKWebView()
        view = webView
    }

    func loadHTMLString(detailItem: Petition) {

        let htmlString = """
<html>
<head>
<meta name = "viewport" content = "width = device-width, initial-scale = 1">
<style> body { font-size: 120%; }
</style>
</head>
<body>
        \(detailItem.body)
</body>
</html>

"""
        webView.loadHTMLString(htmlString, baseURL: nil)
    }
}

//
//  Petitions.swift
//  Project7
//
//  Created by Vrushali Kulkarni on 05/06/20.
//  Copyright © 2020 Vrushali Kulkarni. All rights reserved.
//

import Foundation

struct Petitions: Codable {
    var results: [Petition]
}

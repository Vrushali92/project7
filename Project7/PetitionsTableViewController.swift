//
//  PetitionsTableViewController.swift
//  Project7
//
//  Created by Vrushali Kulkarni on 05/06/20.
//  Copyright © 2020 Vrushali Kulkarni. All rights reserved.
//

import UIKit

final class PetitionsTableViewController: UITableViewController {

    var petitions = [Petition]()
    var filteredPetitons = [Petition]()
    var isfilteredPetitions = false

    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        loadDataFromURL()
    }
}

private extension PetitionsTableViewController {

    func configureUI() {
        title = "Petitions"

        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Credits",
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(creditsTapped))

        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Filter",
                                                           style: .plain,
                                                           target: self,
                                                           action: #selector(filterTapped))
    }

    @objc func creditsTapped() {

        let alertController = UIAlertController(title: "Message",
                                                message: "The information comes from We The People API of the Whitehouse",
                                                preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default)
        alertController.addAction(action)
        present(alertController, animated: true, completion: nil)
    }

    @objc func filterTapped() {

        isfilteredPetitions = true
        let alertController = UIAlertController(title: "Enter petitions",
                                                message: "",
                                                preferredStyle: .alert)
        alertController.addTextField()
        let action = UIAlertAction(title: "OK", style: .default) { [weak self, weak alertController] _ in

            guard let weakSelf = self,
                let text = alertController?.textFields?[0].text else { return }
            self?.filteredPetitons = weakSelf.petitions.filter { $0.title.contains(text) }
            self?.tableView.reloadData()
        }
        alertController.addAction(action)
        present(alertController, animated: true, completion: nil)

    }

    func showAlert() {

        let alertController = UIAlertController(title: "Enter petitions",
                                                message: "",
                                                preferredStyle: .alert)
        alertController.addTextField()
        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertController.addAction(action)
        present(alertController, animated: true, completion: nil)

    }

    func loadDataFromURL() {

        let urlString: String

        if navigationController?.tabBarItem.tag == 0 {
            urlString = "https://www.hackingwithswift.com/samples/petitions-1.json"
        } else {
            urlString = "https://www.hackingwithswift.com/samples/petitions-2.json"
        }

        guard let url = URL(string: urlString) else { return }

        do {
            let data = try Data(contentsOf: url)
            parse(data)
        } catch {
            print(error)
        }

        showError()
    }

    func showError() {
        let ac = UIAlertController(title: "Loading error",
                                   message: "There was a problem loading the feed; please check your connection and try again.",
                                   preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
    }

    func parse(_ data: Data) {

        let decoder = JSONDecoder()

        do {
            let petitionsJSON = try decoder.decode(Petitions.self, from: data)
            petitions = petitionsJSON.results
            filteredPetitons = petitions
            tableView.reloadData()

        } catch {
            print(error)
        }
    }
}

extension PetitionsTableViewController {

    override func tableView(_ tableView: UITableView,
                            numberOfRowsInSection section: Int)
        -> Int {

            return filteredPetitons.count
    }

    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath)
        -> UITableViewCell {

            let cell = tableView.dequeueReusableCell(withIdentifier: "PetitionsCell", for: indexPath)
            cell.textLabel?.text = filteredPetitons[indexPath.row].title
            cell.detailTextLabel?.text = filteredPetitons[indexPath.row].body
            return cell
    }

    override func tableView(_ tableView: UITableView,
                            didSelectRowAt indexPath: IndexPath) {

        guard !petitions[indexPath.row].body.isEmpty else { return }
        let viewController = DetailViewController(detailItem: petitions[indexPath.row])
        navigationController?.pushViewController(viewController, animated: true)
    }
}

//
//  Petition.swift
//  Project7
//
//  Created by Vrushali Kulkarni on 05/06/20.
//  Copyright © 2020 Vrushali Kulkarni. All rights reserved.
//

import Foundation

struct Petition: Codable {

    var title: String
    var body: String
    var signatureCount: Int
}
